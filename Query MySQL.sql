-- SELECT ALL
SELECT * FROM employees

-- ## EX 1 Query que retorna a quantidade de funcionários separados por sexo.
SELECT gender AS Genero, COUNT(gender) AS Quantidade FROM employees GROUP BY gender

-- ## EX 2 Query que retorna a quantidade de funcionários distintos por sexo, ano e ano de nascimento. 
-- Nascimento
SELECT birth_date AS Nascimento, COUNT(*) AS Quantidade FROM employees GROUP BY birth_date
-- Ano
SELECT hire_date AS Contratacao, COUNT(*) AS Quantidade FROM employees GROUP BY hire_date
-- Genero
SELECT gender AS Genero, COUNT(gender) AS Quantidade FROM employees GROUP BY gender

SELECT * FROM salaries
-- ## EX 3 Query que retorna a média, min e max de salário por sexo.
-- Media
SELECT(
SELECT AVG(salary) FROM salaries WHERE emp_no IN (SELECT emp_no FROM employees WHERE gender = 'F')) AS MediaFeminina,
(SELECT AVG(salary) FROM salaries WHERE emp_no IN (SELECT emp_no FROM employees WHERE gender = 'M')) AS MediaMasculina
-- Min
SELECT(
SELECT MIN(salary) FROM salaries WHERE emp_no IN (SELECT emp_no FROM employees WHERE gender = 'F')) AS MinFeminina,
(SELECT MIN(salary) FROM salaries WHERE emp_no IN (SELECT emp_no FROM employees WHERE gender = 'M')) AS MinMasculina
-- Max
SELECT(
SELECT MAX(salary) FROM salaries WHERE emp_no IN (SELECT emp_no FROM employees WHERE gender = 'F')) AS MaxFeminina,
(SELECT MAX(salary) FROM salaries WHERE emp_no IN (SELECT emp_no FROM employees WHERE gender = 'M')) AS MaxMasculina