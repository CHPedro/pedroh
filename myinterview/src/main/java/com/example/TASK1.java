package com.example;

/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */

public class TASK1 {
	
	// retorna verdadeiro se o texto for palindrome e falso caso contrario.
	//recebe string por parametro
	public boolean testPalindrome(String texto) {
		int tamanho = 0;//tamanho do texto
		boolean vf = true;//verificacao se texto e palindrome
		try {
			tamanho = texto.length();
		}
		catch(Exception e) {
			vf = false;//texto nao e palindrome
		}
		
		if(texto == null) {
			vf = false;//nao ha texto
		}
		else {
			//verifica se caracteres sao iguas, a partir das extremidades do texto
			//caso o texto possua um numero impar de caracteres, nao ha necessidade de verificar o caractere do centro
			for(int i = 0; i <= (tamanho / 2); i++) {
				if(texto.charAt(i) != texto.charAt((texto.length()-i)-1)) {
					vf = false;
				}
			}
		}
		return vf;
    }
}
