package com.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */

public class TASK3 {
	
	public void tsk(){
		List<String>  lista;
		lista = new ArrayList<String>();
		
		Random gen = new Random(); 
		int aleatorio = gen.nextInt(49) + 1;
		
		for(int i = 0; i <= aleatorio; i++) {
			lista.add("numero "+i);
		}
		System.out.print(""+lista.size());
	}
}
