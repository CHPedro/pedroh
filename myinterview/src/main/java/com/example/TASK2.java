package com.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 * 
 */

public class TASK2 {

	public void exLista() throws IOException {
		String str = "";
		List<String>  lista;
		lista = new ArrayList<String>();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		//add itens na lista
		do {
		str = br.readLine();
			if(!str.equals("fim")) {
				lista.add(str);
			}
		}while(!str.equals("fim"));//ao digitar 'fim'. a entrada de itens na lista � finalizada
		
		//print list/remove the element in the middle/print list
		
		//print1
		for (int i = 0; i < lista.size(); i++) {
			System.out.println(lista.get(i));
		}
		//remove
		lista.remove((lista.size()-1)/2);
		//print2
		for (int i = 0; i < lista.size(); i++) {
			System.out.println(lista.get(i));
		}
	}
}
